#addin "Cake.FileHelpers"
#addin "Cake.Plist"
#addin "Cake.Xamarin"
#addin "Cake.HockeyApp"

var assemblyInfoFile = File("../src/AssemblyInfo.cs");
var plistFile = File("../src/XamarinEvolve.iOS/Info.plist");
var projectFile = File("../src/XamarinEvolve.iOS/XamarinEvolve.iOS.csproj");
var solutionFile = File("../src/XamarinEvolve.sln");


// should MSBuild treat any errors as warnings.
var treatWarningsAsErrors = "false";

// Parse release notes
var releaseNotes = ParseReleaseNotes("../RELEASENOTES.md");

// Get version
var version = releaseNotes.Version.ToString();
var epoch = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
var local = BuildSystem.IsLocalBuild;
//var semVersion = local ? string.Format("{0}.{1}", version, epoch) : string.Format("{0}.{1}", version, epoch);
var semVersion = string.Format("{0}.{1}", version, epoch);

var build = Argument("build", "Build");
var deploy = Argument("deploy", "Deploy");
var pathToYourPackageFile = File("./bin/iPhoneSimulator");

Task("Deploy")
    .IsDependentOn("Build")
    .IsDependentOn("RestorePackages")
    .IsDependentOn("RestoreComponents")
    .IsDependentOn("UpdateAssemblyInfo")
    .IsDependentOn("UpdateApplePlist")
    .Does(() =>
    {
        UploadToHockeyApp( pathToYourPackageFile, new HockeyAppUploadSettings
        {
            ApiToken = "af5e1cb1dafd4e85b52149205b1f0d2f",
            AppId = "com.jaeyow.app.triathlonplanner-ios",
            Version = semVersion,
            ShortVersion = semVersion,
            Notes = "Uploaded via continuous integration."
        },
        string.Empty);
    });

Task("Build")
    .IsDependentOn("RestorePackages")
    .IsDependentOn("RestoreComponents")
    .IsDependentOn("UpdateAssemblyInfo")
    .IsDependentOn("UpdateApplePlist")
    .Does (() =>
{

    DotNetBuild(projectFile, settings =>
      settings.SetConfiguration("Debug")
          .WithProperty("Platform", "iPhoneSimulator")
          .WithProperty("OutputPath", "bin/Debug/iPhoneSimulator/")
          /*.WithProperty("BuildIpa", "true")*/
          .WithTarget("Build")
          .WithProperty("TreatWarningsAsErrors", treatWarningsAsErrors));

    /*DotNetBuild(projectFile, settings =>
      settings.SetConfiguration("AppStore")
          .WithProperty("Platform", "iPhone")
          .WithProperty("OutputPath", "bin/AppStore/")
          .WithProperty("TreatWarningsAsErrors", treatWarningsAsErrors));*/
});

Task("UpdateApplePlist")
    .Does (() =>
{
    dynamic plist = DeserializePlist(plistFile);

    plist["CFBundleShortVersionString"] = version;
    plist["CFBundleVersion"] = semVersion;

    SerializePlist(plistFile, plist);
});

Task("UpdateAssemblyInfo")
    .Does (() =>
{
    CreateAssemblyInfo(assemblyInfoFile, new AssemblyInfoSettings() {
        Product = "Jose Reyes",
        Version = version,
        FileVersion = version,
        InformationalVersion = semVersion,
        Copyright = "Copyright (c) Jose Reyes"
    });
});

Task("RestoreComponents")
    .Does(() =>
{
    /*var username = EnvironmentVariable("XAMARIN_USERNAME");
    if (string.IsNullOrEmpty(username))
    {
        throw new Exception("The XAMARIN_USERNAME environment variable is not defined.");
    }

    var password = EnvironmentVariable("XAMARIN_PASSWORD");
    if (string.IsNullOrEmpty(password))
    {
        throw new Exception("The XAMARIN_PASSWORD environment variable is not defined.");
    }*/

    RestoreComponents(solutionFile, new XamarinComponentRestoreSettings()
    {
        ToolPath = "./tools/xpkg/xamarin-component.exe",
        Email = "jaeyow@gmail.com",
        Password = "Volleyball1"
    });
});

Task("RestorePackages")
    .Does (() =>
{
    NuGetRestore(solutionFile);
});

RunTarget(build);
